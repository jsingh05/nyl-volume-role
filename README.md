Aero-Volume-Role
=========

This role utilizes the NetApp API to create, update, and delete volumes on a connected NetApp Cluster. The latest documentaion on the Ansible/NetApp API used by this role can be found at: https://docs.ansible.com/ansible/latest/collections/netapp/ontap/na_ontap_volume_module.html.

Requirements
------------

This role requires:

1. Python, Version 3.6.9
2. netapp.ontap, Version 21.10.0
3. netapp-lib, Version 2021.6.25

Role Variables
--------------

*netapp_hostname:*

   - This variable should be set to the NetApp hostname that is used to manage the volumes for this inventory file
   
*netapp_username:*

   - This variable should be set to the NetApp username that is used to manage the volumes for this inventory file
   
*netapp_password:*

   - This variable should be set to the NetApp password that is used to manage the volumes for this inventory file

*provision_type:*

   - This is the execution tag that is associated with each execution, and can be set with "create", "update", or "delete". This will default to "create"

*export_policy_name:*

   - This variable will set the export policy for the managed volumes

*volume_size_unit:*

   - This variable controls what size unit will be used when managing volume sizes
   
*unix:* (optional)

   - This variable will set the unix permissions of the managed volumes

*user_id:* (optional)

   - This variable will set the UNIX user ID for the volume
   
*group_id:* (optional)

   - This variable will set the UNIX group ID for the volume

*volume_size:*

   - This variable will set the size of the volume, in the units that are set by the volume_size_unit variable set initially in the group_vars volumes file. If the volume has already been created and is the source volume for a snapmirror relationship, then by using the "provision_type=update" flag, both the source and destination volumes' size will update to this value

*storage_aggregate:*

   - This variable will set the aggregate that the volume will exist on
   
*svm:*

   - This variable will set the VServer that will be used for this volume

*snapshot_policy:*

   - This variable will set the snapshot policy for this volume. Defaults to 'default'

*space_guarantee:* (optional)

   - This variable will set the space guarantee style for the volume (none, file, volume). Defaults to 'none'

*space_slo:* (optional)

   - This variable will set the space SLO type for the volume (none, thick, semi-thick). This parameter is not supported on Infinite Volumes, and when ommitted the volume will be thin provisioned

*junction_path:* (optional)

   - This variable will set the junction path for the volume

*volume_security_style:* (optional)

   - This variable will set the security style associated with this volume (mixed, ntfs, unified, or unix)

*type:* (optional)

   - This variable will set the volume type for the volume, either read-write (RW) or data-protection (DP)

*percent_snapshot_space:* (optional)

   - This variable will set the amount of space reserved for snapshot copies of the volume

*language:*

   - This variable will set the language for the volume, and defaults to "C.UTF-8"

*comment:* (optional)

   - This variable will set a comment associated with the volume

*autosize_mode:*

   - This variable will set the autosize mode for the volume

*mirror_grow_flag:*

   - This variable will enable the relationship between volumes and snapmirrors that will grow destination volumes whenever the corresponding source volume's size is updated
   
*ready_for_deletion:*

   - This variable acts as a flag to denote if this volume is ready for deletion. If set to false, then when the deletion task execution is ran this host will be skipped over


Example Playbook
----------------

    ---
    - name: Manage volume
      hosts:
        - all
      gather_facts: false
      become: false
      vars_files:
        - vars/ontap_creds.yml
        - vars/volumes.yml
      serial: 1

      roles:
        - aero-volume-role


Author Information
------------------

This role was developed by Enterprise Vision Technologies (*www.evtcorp.com*)
